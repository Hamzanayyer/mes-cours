# Activer Cors pour React

Pour utiliser sa propre API il faut utiliser Cors :

1. installer Cors `npm i -s cors` puis `npm i -D @types/cors`

````
import cors 'from cors';
app.use(cors());
````
