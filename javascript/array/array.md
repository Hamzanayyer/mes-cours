# Les Tableaux (array)

Pour rassembler plusieurs éléments, des objets par exemple, on peut les stocker dans un tableau.

````javascript
const tab = [me, user, user2, user3, user4, user5];
console.log(tab);
````

On peut aussi faire un `console.table(tab);` à la place du console.log. La console affichera un tableau avec ses éléments.

## Length et index

**Pour demander un élément du tableau à l'aide de son index on indique l'index entre des crochets :**
````javascript
console.log(tab[0]);
````
 
**Pour connaître la longeur du tableau :**
````javascript
console.log(tab.length);
````

**Pour obtenir le dernier élément d'un tableau :**
````javascript
console.log(tab[tab.length - 1]);
````

## La méthode forEach

Il s'agit d'une fonction qui permet de manipuler un array en le parcourant.
````javascript
const tab = [user, user2, user3, user4, user5];

tab.forEach(user => {
console.log('user: ', user)
});
````
 
forEach va parcourir les éléments du tableau un par un et les afficher.

## La méthode Map et le passage par référence

Elle fonctionne de la même façon que l'operateur forEach. La différence c'est que Map renvoit un nouveau tableau avec le résultat demandé à l'aide de la fonction.
````javascript
const array1 = [1, 4, 9, 16];

const map1 = array1.map(x => x * 2);

console.log(map1);
````
La console va renvoyer `[2, 8, 18, 32]`. La méthode map créer un nouveau tableau (tab2) à partir du premier (tab).

**Pour ajouter un élément au tableau :**
````javascript
const tab2 = tab.map((user, index) => {
    user.index = index;
    return user;
});

console.table(tab2);
````
On a modifié le tableau en lui ajoutant une propriétée index. Alors, en plus de créer un nouveau tableau, le tableau de référence (tab) sera modifié aussi. Pour remédier à ce problème il faut écrire le code comme ceci :
````javascript
const tab2 = tab.map((user, index) => {
    const temp = {...user};
    temp.index = index;
    return temp;
});
````
Pour créer le nouvel objet on rajoute les trois petits points comme dans le code ci-dessus. Cela va copier le tableau de base et c'est cette copie qui sera modifiée et non pas le tableau d'origine.

## La méthode filter

Il s'agit d'une méthode qui va filtrer les éléments d'un tableau et en renvoyer un autre.

**Pour afficher les personnes qui ont plus de 30 ans :**
````javascript
const filteredArray = tab.filter(user => user.age > 30);
console.table(filteredArray);
````

On peut utiliser plusieurs méthodes à la fois :
````javascript
const filteredArray = tab
    .filter(user => user.age > 30);
    .map(user => {
        user.lastName = user.lastName + 's';
        return user;
    });
console.table(filteredArray);
````

Cela va filtrer les gens en affichant ceux qui ont plus de 30 ans, et cela va ajouter un "s" a la fin de leur nom.

On peut également écrire ce code en utilisant des fonctions (c'est ce qu'on appelle la programmation fonctionnelle) :
````javascript
const filteredArray = tab
    .filter(isUnder30)
    .map(addSAtEnd)
;

function isUnder30(user) {
    return user.age > 30;
};

function addSAtEnd(user) {
    user.lastName = user.lastName + 's';
    return user;
}
 ````

`La programmation fonctionnelle` = Enchainer des fonctions sur des méthodes.

PS : `Il est possible de déclarer une fonction après, mais de l'utiliser avant comme dans le code ci-dessus.`

## La méthode Sort

Il s'agit d'une méthode qui compare les éléments pour les trier.

**Pour trier les gens en fonction de leur âge (du plus jeune au plus vieux) :**
````javascript
const sortedArray = tab.sort(user1, user2) =>
    return user1.age - user2.age);

console.table(sortedArray);
````

Si on inverse les deux users, cela va inverser le trie, et afficher les plus vieux d'abord.

**Pour trier par ordre alphabétique :**
````javascript
const sortedArray = tab.sort(user1, user2) => {
    if (user1.firstName < user2.firstName) {
        return -1;
    } else {
        return +1;
    }
});

console.log(sortedArray);
````

PS : `Quand on a une structure conditionnelle aussi simple on peut l'écrire d'une façon plus simple :`
````javascript
const sortedArray = tab
.sort(user1, user2) => user1.firstName < user2.firstName ? -1 : +1;   
````

## La méthode includes

La méthode includes permet de déterminer si un tableau contient une valeur.

**Pour afficher les personnes qui ont un a ou A dans leur prénom :**
````javascript
const sortedArray = tab
    .filter(user => user.firstName.includes('a') || user.firstName.includes('A');
````

## La méthode push

Elle sert à ajouter un élément à la fin d'un tableau.

**Pour créer un tableau vide et ajouter des éléments à ce tableau :**
````javascript
const tab2 = [];

const baby1 = {
    name: 'basile'
};

const baby2 = {
    name: 'achi'
}

tab2.push(baby1, baby2);

console.table(tab2);
````

## La méthode Concat

La méthode concat est utilisée afin de fusionner un ou plusieurs tableaux en les concaténant. Cette méthode ne modifie pas les tableaux existants, elle renvoie un nouveau tableau qui est le résultat de l'opération.

````javascript
const tab2 = [1, 2, 3];
const tab3 = [4, 5, 6];

const tab4 = tab2.concat(tab3);

console.table(tab4);
````

On peut aussi procéder comme ceci :
````javascript
const tab2 = [1, 2, 3];
const tab3 = [4, 5, 6];

const tab4 = [...tab2, ...tab3];

console.table(tab4);
````

## La méthode splice

Elle permet de modifier le contenu d'un tableau en retirant des éléments et/ou en ajoutant de nouveaux éléments à meme le tableau. On peut donc vider ou remplacer une partie d'un tableau, mais le tableau de base sera modifié aussi.

Ajouter un élément 6 avec splice :
````javascript
const tab2 = [1, 2, 3, 4, 5, 7];

tab2.splice(start:4, deletCount:0, items:6);
console.log(tab2);
````

Ici le 4 représente l'index (l'endroit ou on veut faire l'ajout) c'est à dire après le 5. Le 0 indique qu'on ne veut rien supprimer, et le 6 l'élément qu'on veut rajouter.

**Pour supprimer un élément à partir du 5 eme :**

````javascript
const tab2 = [1, 2, 3, 4, 5, 7];

tab2.splice(start:5, deletCount:1);
console.log(tab2);
````

## La méthode slice

Il créée un nouveau tableau en copiant une partie du tableau de base sans le modifier.

Copier les éléments 2 a 4 et les mettre dans le nouveau tableau :
````javascript
const tab2 = [1, 2, 3, 4, 5, 6];

const tab3 = tab2.slice(1, 4);
console.log(tab3);
console.log(tab2);
````
On copie à partir de l'index 1 jusqu'à l'index 4 (l'index 4 ne sera pas copié).

## La méthode reduce 

C'est une méthode qui permet d'accumuler des éléments.

````javascript
const tabNumber = [2, 5, 10, 2, 33];

const total = tabNumber.reduce(acc, currentValue) => acc = acc + currentValue, 0);
console.log(total);
````
Le résultat est égal à 52.

> **Exemple avec un objet :**

````javascript
const tabNumber = [
    {firstName: 'toto', age: 12, hobbies: ['foot', 'playstation']},
    {firstName: 'tata', age: 13, hobbies: ['foot', 'playstation']},
    {firstName: 'titi', age: 14, hobbies: ['foot', 'playstation']},
    {firstName: 'tutu', age: 15, hobbies: ['foot', 'playstation']},
]

const total = tabNumber
.reduce((acc, currentValue) => acc.concat(currentValue.hobbies), []);
console.log(total);
````

> **Une autre méthode :**
````javascript
const tabNumber = [
    {firstName: 'toto', age: 12, hobbies: ['foot', 'playstation']},
    {firstName: 'tata', age: 13, hobbies: ['foot', 'playstation']},
    {firstName: 'titi', age: 14, hobbies: ['foot', 'playstation']},
    {firstName: 'tutu', age: 15, hobbies: ['foot', 'playstation']},
]

const total = tabNumber.reduce((acc, currentValue) => [...acc, ...currentValue.hobbies], []);

console.log(total);
````

## La méthode indexOf

Elle renvoie l'index d'un élément à partir d'un indice.

````javascript
const beasts = ['ant', 'bison', 'camel', 'duck', 'bison'];

console.log(beasts.indexOf('bison'));
````

La console devrait renvoyer 1.

## La méthode find

Elle permet de trouver un élément dans le tableau :

````javascript
const hotelToFind = hostels.find(hotel => hotel.name === "hotel ocean");

console.log(hotelToFind);
````

La console renvoie l'élément trouvé : hotel ocean.

**Pour trouver un prédicat :**

````javascript
const hotelToFind = hostels.find(hotel => hotel.pool === true);

console.log(hotelToFind);
````

La console devrait renvoyer le premier élément true seulement et pas les autres true. En effet, le premier true est trouvé et renvoyé, puis la fonction s'arrete.

`PS : Dans tous les cas on ne peut pas enchaîner avec d'autres opérateurs si on utilise un find.`

## La méthode every

C'est une méthode qui renvoit un boolean, si chaque hotel de la liste a bien la valeur pool = true, every va renvoyer true, si un seul ne respecte pas le prédicat il va renvoyer false.

````javascript
const allHostelsHavePools = hostels.every(hostel => hostel.pool === true);

console.log(allHostelsHavePools);
````

La console devrait renvoyer false car un hotel de la liste n'a pas de pool, si toutes les pools étaient à true la console aurait renvoyer true.

## La méthode some

C'est une méthode qui renvoie un boolean, si au moins un est true ça va renvoyer true

````javascript
const allHostelsHavePools = hostels.some(hostel => hostel.pool === true);

console.log(allHostelsHavePools);
````

La console devrait renvoyer true. Si on avait mis `=== false` il aurait renvoyer true aussi, car dans la liste il y en a au moins un qui est false.
