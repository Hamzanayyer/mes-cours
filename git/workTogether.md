# Travailler à plusieurs sur un projet

## Récupérer, travailler et envoyer son travail

1. Cliquer sur la fleche bleue pour récuperer la master
2. Créer une branche pour apporter des modifications au projet
3. Faire les modifications
4. Commiter et pusher
5. Aller sur gitlab et cliquer sur create merge request
6. Choisir le responsable qui va gérer les conflits
7. Cliquer sur `submit`, le projet a été envoyé

## Récupération du travail chez le chef de projet

1. Reception d'une notification qui prévient le chef de l'arrivée de la request
2. Examens du code en cliquant sur changes et validation si tout va bien en cliquant sur `merge`
3. En cas de conflit on clique sur resolve conflict
4. Le chef écrit au développeur et demande de resoudre les conflits

## Récéption de l'information chez le développeur

1. Se mettre sur master et clicker sur la fleche bleue pour récuperer la dernière version
2. Retourner sur sa branche, cliquer sur vcs git et cliquer sur merge changes
3. Cliquer sur master et la merger dans la branche
4. Résoudre les conflits en cliquant sur le fichier puis sur merge un par un
5. Une fenêtre apparait : à gauche ma version à moi, à droite la version du collègue
6. Modifier le code et cliquer sur apply
7. Signaler que la merge a été faite en appuyant sur shift + commande + k (ou faire un push)
8. Pusher, puis sur Gitlab cliquer sur merge dans la request