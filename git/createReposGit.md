# Créer un repository Git

Gitlab est un site alors que Git est un protocole pour échanger des données.

## Étapes

1. Une fois Git installé, il faut ouvrir le projet dans Webstorm et ouvrir le terminal de commande.
2. Saisir `git init`
3. Selectionner les fichiers et cliquer sur add
4. Cliquer sur la fleche verte en haut à doite
5. Décocher les cases à droite
6. Saisir le commit message
7. Cliquer sur `Commit and push` (On sauvegarde sur la machine et on envoie dans le cloud)
8. Définir un repo sur gitlab
9. Toujours sur gitlab, créer un projet
10. Cliquer sur clone, copier le https
11. Sur Webtorm, cliquer sur define remote
12. Coller et cliquer sur push
