# Créer des branches

`master` = projet de référence (de base)

## Étapes
1. Pour que nos modification soient intégrées a la master, il faut cliquer en bas sur `Git: master`
2. Créer une nouvelle branche
3. On peut passer d'une version à l'autre avec `checkout`
4. Il faut commiter avant de checkout la branche
5. Une fois la branche créee, il faut demander l'autorisation pour l'envoyer vers la master
6. Aller sur git et cliquer sur create merge request
7. Verifier si on va de branch vers master
8. On peut s'assigner pour faire la demande au chef
9. Enfin pour recuperer la modification on retourne sur webstorm, puis sur la master, on clique sur update (fleche bleue) et on clique sur merge.
