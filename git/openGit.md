# Ouvrir un projet Git existant

## Etapes

1. Aller sur le projet sur Git
2. Cliquer sur clone et copier le https
3. Sur Webstorm, cliquer sur `VCS` puis `get from version control`
4. Cliquer sur clone sur git et copier le lien https
5. le fichier est importé