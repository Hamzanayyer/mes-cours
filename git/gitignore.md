# Git ignore

Pour éviter d'envoyer certains éléments inutiles sur git (nodemodule, package.json etc).
Il faut faire un gitignore au tout début, lors de la création du projet.
Si on rajoute un fichier avant le git ignore il ne sera pas ignoré.

## Les étapes

1. Créer un fichier dans la racine appelé `.gitignore` puis l'ajouter à git (.nodemodule par exemple)
2. Dans ce fichier, mettre tous les fichiers et dossiers que l'on ne veut pas en commencant par un .
3. Pour retirer un fichier (idea par exemple) qui n'a pas été ignoré, Saisir `git rm -rf .idea`
4. On refait commit, idea aura disparu

## Ignorer tous les fichiers qui finissent par txt

monfichier/*.txt
