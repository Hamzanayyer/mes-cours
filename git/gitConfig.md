# Configuration initiale de Git

## Créer un dépôt Git localement

Nous avons Git installé, et nous souhaitons maintenant commencer à l'utiliser.

Nous allons créer un dépôt local Git dans un dossier vide par exemple test-git :

`cd test-git && git init`
La commande git init permet d'initialiser un nouveau dépôt Git.

En tapant cette commande dans un dossier, vous aurez le message suivant :

Dépôt Git vide initialisé dans /chemin/absolu/vers/le/dossier/caché/git
Git va créer un dossier caché dans le dossier dans lequel vous avez initialisé le dépôt.

Vous pouvez maintenant voir ce dossier en tapant :

`ls -a`
ls, abréviation de list, permet de lister les fichiers et les dossiers dans le répertoire courant.

L'option -a permet d'afficher tous les fichiers et dossiers, y compris les fichiers et dossiers cachés.

Dans ce dossier caché nous trouvons les dossiers branches, hooks, info, objects, refs et les fichiers config, description et HEAD.

Nous les verrons également au fur et à mesure, nous avons simplement vu le fichier config qui est le fichier de configuration locale du dépôt.

Configuration basique de Git
Git possède une commande permettant de le configurer :

`git config`

## Les niveaux de la configuration Git
Il existe trois manière de configurer Git :

1. sans option : si vous lancez la commande dans un répertoire Git, alors les configurations ne seront valables que pour le dépôt en cours d'utilisation. La configuration se trouvera alors dans le dossier du dépôt dans .git/config.

2. avec l'option --global : si vous ajoutez cette option à la commande, alors la configuration se fera pour votre utilisateur sur votre machine : par exemple Paul ou root. Le fichier de configuration se trouvera alors ici : ~/.gitconfig (ou C:\Documents and Settings\utilisateur ou C:\Users\utilisateur sur Windows).

3. avec l'option --system : si vous utilisez cette option, alors la configuration sera valable pour tous les utilisateurs du système. Le fichier de configuration se trouvera alors ici : /etc/gitconfig. (ou sur WIndows dans C:\ProgramData\Git\config).

Git va lire les configuration dans cet ordre système puis globale puis locale au dépôt.

Git va cumuler les configurations trouvées, mais si deux propriétés ont deux valeurs différentes, alors Git prendra la plus spécifique.

Par exemple, si vous spécifiez une adresse email au niveau global et une au niveau local dans la configuration de Git, il prendra l'adresse email au niveau local lorsque vous utiliserez Git dans le dépôt.

## Configuration de l'identité

Nous allons commencer par voir comment configurer son nom et prénom et son adresse email.

Ces informations seront liées à chaque sauvegarde que vous effectuerez avec Git.

Par exemple, pour définir ses nom, prénom et adresse email pour l'utilisateur courant, il faut faire :

``git config --global user.name "Jean Dupont"
git config --global user.email jean.dupont@protonmail.com``

## Lister les configurations
Vous pouvez lister à tout moment la configuration de Git en faisant :

``git config --list``
Si vous ne vous trouvez pas dans un dépôt Git, seules les configurations systèmes et globales s'afficheront, par exemple :

``user.name=Jean Dupont
user.email=jean.dupont@protonmail.com
core.autocrlf=input``
Si vous vous trouvez dans un dépôt, toutes les configurations système, globale et locale fusionnées suivant la règle spécifié ci-dessus apparaîtront :

``user.name=Jean Dupont
user.email=jean.dupont@protonmail.com
core.autocrlf=input
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=git@gitlab.com:jdupont/megaprojet.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
branch.develop.remote=origin
branch.develop.merge=refs/heads/develop
gui.wmstate=normal
gui.geometry=1822x800+2605+53 612 212
branch.ng9.remote=origin
branch.ng9.merge=refs/heads/ng9
branch.ng9-2.remote=origin
branch.ng9-2.merge=refs/heads/ng9-2``
Nous verrons au fur à mesure ce que signifie cette configuration. Mais il faut simplement que vous reteniez le principe des niveaux de configuration Git pour le moment.

A noter qu'il est complètement inutile de s'attarder sur les options de configuration avancées de Git, il en existe plusieurs centaines mais vous n'en aurez jamais besoin.

## Lister la configuration pour une propriété
Vous pouvez également vérifier uniquement un paramètre en tapant dans un terminal git config paramètre, par exemple :

git config user.email
Qui donnerait dans notre exemple :

jean.dupont@protonmail.com
