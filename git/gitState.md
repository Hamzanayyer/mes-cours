# Les état d'un fichier

Lorsqu'un fichier est suivi par Git, il peut être dans trois états :

1. modifié : le fichier est modifié mais la version modifiée n'est pas encore indexée ou sauvegardée.

2. indexé : le fichier est suivi par Git et ajouté en zone de transit (staging area). Git utilise, comme nous le verrons en détails, un fichier d'index pour déterminer quels fichiers feront parties de la prochaine sauvegarde.

3. validé : le fichier est stocké dans la base de données locale Git, qui constitue avec les fichiers de configuration le dépôt local.

Nous allons bien sûr voir ces états en détails et comment passer les fichiers d'un état à un autre.
