# Configurer les paramètres Git

## Étapes

1. Ouvrir le terminal de commande
2. Saisir son username:
`**git config --global user.name "FIRST_NAME LAST_NAME"**`
3. Saisir son adresse email:
`**git config --global user.email "MY_NAME@example.com"**`
4. Dans settings, cliquer sur plugin et installer gitlab projects`
5. Redémarrer Webstorm
6. Retourner dans settings, puis cliquer sur version control et aller sur gitlab
7. Saisir https://gitlab.com dans la 1ere ligne
8. Saisir gitlab.com dans la 2 ème
9. Cliquer sur token, puis sur API et créer le token
10. Saisir le token
11. Git est activé, saisir git init dans le terminal de commande pour s'en assurer
