# HAMZA NAYYER

* Age : 26 ans
* Mail : hamzanayyer@gmail.com
* Adresse : 42 t rue Ambroise Thomas 93700 Drancy
* Num : 06.59.64.40.42

--- 

## Diplômes

* 2020 : White rabbit Formation
> Développeur Javascript
* 2013 : Baccalauréat Professionnel
> Comptabilité et Gestion

--- 

## Expériences

* 2019 - 2020 : **Chargé de clientèle** - Competence Call Center
> _Identiﬁcation des clients via des appels vidéo entrants pour des banques en ligne (Banque N26 et Banque international à Luxembourg) - Lutte contre les fraudes et les usurpations d'identité - Collecte de données sensibles et saisie dans le système Idnow_ 

* 2018 - 2019 : **Manager** - Monop'
> _Accueil et ﬁdélisation de la clientèle - Management d'une équipe de 5 personnes - Gestion des commandes et des stocks - Implantations des rayons - Étiquetage - Gestion des caisses et des prélèvements - Ouvertures et fermetures du magasin_

* 2013 - 2017 : **Employé polyvalent** - Franprix
> _Accueil et encaissement des clients - Traitement des livraisons (contrôle quantitatif et qualitatif) Approvisionnement des rayons et étiquetage des produits - Gestion des commandes et des stocks - Rotation des produits et vériﬁcation des DLC_

---

## Centres d'interet 

* Football
* Jeux-vidéo
* Mangas