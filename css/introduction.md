# Les bases de CSS

## Appliquer le CSS au HTML

Saisir le code suivant dans le head de la page html

````html
<link rel="stylesheet" href="style.css">
````

## Styles principaux

* padding : l'espace intérieur (autour du texte d'un paragraphe)
* margin : l'espace extérieur (autour de l'élément)
* width : la largeur de l'élément
* height : la longeur de l'élément
* background-color : la couleur derrière le contenu de l'élément et son padding
* color : la couleur du contenu de l'élément (le texte)
* text-shadow : affiche une ombre portée sur le texte à l'intérieur de l'élément
* display : définit le mode d'affichage d'un élément
* font-family : définit la police
* font-size : définit la taille
* font-weight : Met en italique, gras etc

### Changer la couleur de la page

````css
html {
  background-color: #00539F;
}
````

### Mettre en forme le corp de la page

````css
body {
  width: 600px;
  margin: 0 auto;
  background-color: #FF9500;
  padding: 0 20px 20px 20px;
  border: 5px solid black;
}
````

* Pour margin, lorsqu'on a deux valeurs pour une propriété la première s'applique au haut et au bas et la seconde à gauche et à droite.
* Lorsqu'on a quatre valeurs comme pour le padding, la première s'applique en haut, puis la seconde à gauche et ainsi de suite.

### Centrer une image

````
img {
  display: block;
  margin: 0 auto;
}
````

### Le positionnement 

La propriété position définit la façon dont un élément est positionné dans un document. Les propriétés top, right, bottom et left déterminent l'emplacement final de l'élément positionné.

#### Positionnement relatif

L'élément est positionné dans le flux normal du document puis décalé, par rapport à lui-même, selon les valeurs fournies par top, right, bottom et left.

> HTML
````html
<div class="box" id="un">Un</div>
<div class="box" id="deux">Deux</div>
<div class="box" id="trois">Trois</div>
<div class="box" id="quatre">Quatre</div>
````

> CSS
````css
.box { 
   display: inline-block;
   background: red;
   width: 100px;
   height: 100px;
   color: white;
}

#deux { 
   position: relative; 
   top: 20px; 
   left: 20px; 
}
````

#### Positionnement absolute

L'élément est retiré du flux normal et aucun espace n'est créé pour l'élément sur la page. Il est ensuite positionné par rapport à son ancêtre le plus proche qui est positionné s'il y en a un ou par rapport au bloc englobant initial sinon. La position finale de l'élément est déterminée par les valeurs de top, right, bottom et left.

> HTML
````html
<h1>Positionnement absolu</h1>
<p>Un élément de bloc simple. Les éléments de bloc adjacents sont sur de nouvelles lignes en-dessous.</p>
<p class="positioned">Par défaut, on occupe 100% de la largeur de l'élément parent et on est aussi grand que notre contenu.</p>
<p>Nous sommes séparés par nos marges (une seule marge en raison de la fusion des marges).</p>
<p>Les éléments <em>inline</em> <span>comme celui-ci</span> et <span>celui-là</span> se situent sur la même ligne avec également les noeuds texte. S'il y a de l'espace sur la même ligne. Les éléments qui dépassent <span>passent à la ligne si possible — comme pour ce texte.</span> ou cette image <img src="https://mdn.mozillademos.org/files/13360/long.jpg"></p>
````

> CSS
````css
body {
  width: 500px;
  margin: 0 auto;
}

p {
  background: aqua;
  border: 3px solid blue;
  padding: 10px;
  margin: 10px;
}

span {
  background: red;
  border: 1px solid black;
}

.positioned {
  position: absolute;
  background: yellow;
  top: 30px;
  left: 30px;
}
````

### Float

La propriété float indique qu'un élément doit être retiré du flux normal et doit être placé sur le côté droit ou sur le côté gauche de son conteneur. Le texte et les autres éléments en ligne (inline) entoureront alors l'élément flottant. L'élément est retiré du flux normal de la page mais s'inscrit toujours dans le flux (contrairement au positionnement absolu).

> CSS
````css
div {
  border: solid red;
  max-width: 70ex;
} 

h4 {
  float: left;
  margin: 0;
}
````

> HTML
````html
<div>
  <h4>Coucou !</h4>
  Voici du texte. Voici du texte. Voici du texte.
  Voici du texte. Voici du texte. Voici du texte.
  Voici du texte. Voici du texte. Voici du texte.
  Voici du texte. Voici du texte. Voici du texte.
</div>
````
