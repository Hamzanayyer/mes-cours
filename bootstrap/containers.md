# Les Containers

## Différence entre le container fluide et le container classique

`Container` : Le contenu sera centré au milieu de la page

````html
<div class="container">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
````
`Container-fluid` : Le contenu prend toute la page en longueur et en largeur
````html
<div class="container-fluid">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
````
 
## Row et col

Les `row` (ligne) et les `col` (colonne) vont toujours ensemble, on ne peut pas les utiliser seuls.

````html
<div class="container">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
````
On peut créer des row dans des cols mais il faut toujours commencer par une row.
````html
<div class="container">
    <div class="row">
        <div class="col"></div>
            <div class="row">
                <div class="col"></div>
            </div>
        <div class="col"></div>
    </div>
</div>
````
## Taille des col
Il ya 12 colonnes au total. Si on veut deux colonnes dont la première fait un tiers de la page et l'autre deux tiers :
````html
<div class="col-4></div>
<div class="col-8></div>
````

Pour décaler à gauche (dans les colonnes non utilisées) on met `offset-1` :
````html
<div class="col-7 offset-1></div>
````

Si on met un background, il faut mettre un contenu pour voir la couleur.
