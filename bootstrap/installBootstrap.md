# Installer Bootstrap

## Étapes

1. Créer un nouveau projet sur Webstorm
2. Créer les dossiers (assets, main, xp)
3. Ouvrir le terminal et saisir `npm init`
4. Puis mettre entrer à chaque fois jusqu'au package name
5. Entrer le nom (my-cv par ex)
6. Saisir npm install bootstrap
7. Une fois le dossier node module créée, créer le fichier html dans main
8. Relier le fichier bootstrap
````html
<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">
````
9. Créer un container :
````html
<div class="container"> </div>
````
10. Créer à l'interieur du container une grille :
````html
    <div class="row">
        <div class="col numbers">1</div>
        <div class="col numbers">2</div>
        <div class="col numbers">3</div>
    </div>

    <div class="row">
        <div class="col numbers">1</div>
        <div class="col numbers">2</div>
        <div class="col numbers">3</div>
        <div class="col numbers">4</div>
    </div>
````
11. Créer un fichier CSS
 
 12. Relier le CSS au fichier HTML avec le code suivant :
 ````html
 <link rel="stylesheet" href="../assets/styles.css">
````