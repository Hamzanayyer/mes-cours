# Intégrer Bootstrap

Sur le [site de Bootstrap](https://getbootstrap.com/docs/4.4/components/alerts), dans la rubrique documentation/component il y a tous les outils(component) prêts à l'emploi.

## Utiliser un de ces component

* Installer Bootstrap et le relier à la page html avec la balise link
* Ajouter le CSS, le JS et Jquery à la page html
* Intégrer le code du component et lire la documentation pour l'utiliser

