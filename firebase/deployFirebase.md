# Deploy firebase

C'est un outil qui permet de créer des applications mobiles et des sites internet en facilitant la gestion des bases de données et le deploiement.

## Étapes 

1. Aller sur le site de firebase
2. Se connecter et ajouter un projet
3. Sur Webstorm, ouvrir le terminal
4. Saisir `npm install -g firebase-tools`
5. Saisir `firebase --version` pour voir si firebase est accéssible 
6. Créer un fichier qui sera à la racine du projet(page d'acceuil) en le nommant index.html
7. Saisir `firebase init hosting` dans le terminal
8. S'il faut se connecter, saisir `firebase login`
9. Se connecter sur la fenêtre qui s'est ouverte
10. Selectionner le projet
11. Saisir ./
12. Répondre N aux deux prochaines questions
13. Firebase est initialisé
14. Rajouter à firebase.json,package.json et package-lock.json
15. Saisir `firebase deploy`
16. Le site est en ligne (faire inspecter et cocher disable cache si le site ne fonctionne pas)
17. En cas de problème avec le css et le js, il faut créer un dossier et y rajouter le bootstrap (dist) et le jquery (dist)
18. Modifier les fichiers html avec le bon code
19. Saisir firebase deploy