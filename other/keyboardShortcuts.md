# Raccourci Clavier

`ctrl + A` = sélectionner tout le contenu

`ctrl + F` = faire une recherche

`ctrl + R` = rafraîchir la page

`ctrl + tab` = basculer sur l'onglet suivant

`ctrl + shift` + tab = revenir sur l'onglet précédent

`shift + flèche` = sélectionner le texte

`shift + ctrl + flèche` = sélectionner un mot

`ctrl + z` = revenir en arrière 

`shift + ctrl + z` = annuler le retour en arrière

`ctrl + d` = dupliquer

`alt + j` = sélectionner une occurrence 

`ctrl + y` = supprimer une ligne 

`ctrl + x` = couper une ligne

`alt + ctrl + l` = remettre en forme

`ctrl + espace` = voir les choix possibles

`ctrl + s` = enregistrer

`shift + ctrl + flèche haut ou bas` = déplacer la ligne

`shift + f6` = changer le nom partout

`ctrl + -` = réduire la ligne